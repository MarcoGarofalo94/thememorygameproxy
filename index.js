const express = require('express');

const app = express();

const port = 8082;
let ESP_IP = null;
app.get('/init', (req,res,next) => {
    console.log(req.headers.bearer);
    ESP_IP = req.headers.bearer;
    res.send(ESP_IP);
    next();
});

app.get('/connect', (req,res,next) => {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({ ip: ESP_IP }));
    next();
});



app.listen(port,() => console.log('Listening at port '+port));
